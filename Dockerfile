# 基于的镜像
FROM docker.io/adoptopenjdk/openjdk11:latest

ADD target/*.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]

EXPOSE 9001
