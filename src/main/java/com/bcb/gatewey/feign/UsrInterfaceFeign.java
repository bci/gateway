package com.bcb.gatewey.feign;



import com.bcb.commom.form.UsrAuthCheckForm;
import com.bcb.commom.vo.ResultVO;
import com.bcb.commom.vo.user.UsrAuthCheckVO;
import com.bcb.gatewey.feign.fallback.UsrInterfaceFeignFallbackImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


/**
 * @author: Mr.Yuan
 * @date: 2020/04/28
 * @description: 用户模块对外提供接口
 **/
// @FeignClient(name = "user", fallback = UsrInterfaceFeignFallbackImpl.class)
// @FeignClient(name = "user")
@FeignClient(name = "user", fallback = UsrInterfaceFeignFallbackImpl.class)
public interface UsrInterfaceFeign {
    /**
     * @description: 认证鉴权
     * @param form: 认证鉴权参数
     * @return: com.bcb.commom.vo.ResultVO
     */
    @PostMapping(value = "/admin/authCheck")
    ResultVO<UsrAuthCheckVO> authCheck(@RequestBody UsrAuthCheckForm form);

}

