package com.bcb.gatewey.feign.fallback;

import com.bcb.commom.enums.UsrAuthCheckResultEnum;
import com.bcb.commom.form.UsrAuthCheckForm;
import com.bcb.commom.vo.ResultVO;
import com.bcb.commom.vo.user.UsrAuthCheckVO;
import com.bcb.gatewey.feign.UsrInterfaceFeign;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author: Mr.Yuan
 * @date: 2020-06-28
 * @description:
 */

@Service
public class UsrInterfaceFeignFallbackImpl implements UsrInterfaceFeign {
    @Override
    public ResultVO<UsrAuthCheckVO> authCheck(@RequestBody UsrAuthCheckForm form){
        return ResultVO.error(UsrAuthCheckResultEnum.USR_ERROR_AUTH_SERVICE_UNAVAILABLE.getCode(),
                              UsrAuthCheckResultEnum.USR_ERROR_AUTH_SERVICE_UNAVAILABLE.getMsg());
    }
}


