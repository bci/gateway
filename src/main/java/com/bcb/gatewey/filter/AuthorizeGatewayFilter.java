package com.bcb.gatewey.filter;

import java.io.UnsupportedEncodingException;
import java.util.List;

import com.bcb.commom.constant.BcbConstant;
import com.bcb.commom.enums.UsrAuthCheckResultEnum;
import com.bcb.commom.form.UsrAuthCheckForm;
import com.bcb.commom.vo.ResultVO;
import com.bcb.commom.vo.user.UsrAuthCheckVO;
import com.bcb.gatewey.feign.UsrInterfaceFeign;
import com.bcb.gatewey.util.AjaxResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.server.ServerWebExchange;

import com.alibaba.fastjson.JSON;
import com.bcb.gatewey.properties.IgnoreUrlsProperties;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

/**
 * token校验过滤器
 * @author fanhaohao
 *
 */
@Component
@Slf4j
public class AuthorizeGatewayFilter implements GlobalFilter, Ordered {
	// 认证头部字段名称定义
    private static final String AUTHORIZE_TOKEN = "token";
	private static final String AUTHORIZE_TOKEN_PREFIX = "Bearer ";

    // 认证失败返回的code
	private static final Integer FAIL_CODE = -1;

    // UTF-8 字符集
    public static final String UTF8 = "UTF-8";

	@Resource
	private UsrInterfaceFeign usrInterfaceFeign;

	@Autowired
	private IgnoreUrlsProperties ignoreUrlsProperties;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        HttpHeaders headers = request.getHeaders();
		String path = request.getPath().pathWithinApplication().value();

		log.debug("Incoming request, url:{}.", path);

		// 如果url在免认证鉴权名单中，则直接跳过
		if (isIgnoreHttpUrls(path)) {
			return chain.filter(exchange);
		}

		try {
			final String authorization = headers.getFirst(AUTHORIZE_TOKEN);

			if (authorization != null && authorization.startsWith(AUTHORIZE_TOKEN_PREFIX)) {
				// 调用用户服务接口进行认证鉴权
				String authToken = authorization.substring(AUTHORIZE_TOKEN_PREFIX.length());
				UsrAuthCheckForm form = new UsrAuthCheckForm();
				form.setToken(authToken);
				form.setUrl(path);
				ResultVO<UsrAuthCheckVO> r = usrInterfaceFeign.authCheck(form);
				log.debug("feign return：{}", r);

				// 认证鉴权失败
				if (!r.getSuccess()) {
					log.warn("Auth failed: url:{}, result:{}.", form.getUrl(), r.toString());
					return unauthorizedResponse(exchange, r);
				}
				// 认证鉴权成功
				else {
					UsrAuthCheckVO vo = r.getResult();

					log.debug("Auth success: url:{}, userId:{}.", form.getUrl(), vo.getUserId());

					//转发的请求都在头部加上认证鉴权结果信息
					ServerHttpRequest.Builder builder = request.mutate();

					if (null != vo.getUserId()) {
						builder.header(BcbConstant.HEADER_USER_ID, String.valueOf(vo.getUserId()));
					}
					if (null != vo.getSessionId()) {
						builder.header(BcbConstant.HEADER_SESSION_ID, String.valueOf(vo.getSessionId()));
					}
					if (null != vo.getCurrentType()) {
						builder.header(BcbConstant.HEADER_CURRENT_TYPE, String.valueOf(vo.getCurrentType()));
					}
					if (null != vo.getPlatformStaffId()) {
						builder.header(BcbConstant.HEADER_PLATFORM_STAFF_ID, String.valueOf(vo.getPlatformStaffId()));
					}
					if (null != vo.getCurrentShopId()) {
						builder.header(BcbConstant.HEADER_CURRENT_SHOP_ID, String.valueOf(vo.getCurrentShopId()));
					}
					if (null != vo.getCurrentMerchantId()) {
						builder.header(BcbConstant.HEADER_CURRENT_MERCHANT_ID, String.valueOf(vo.getCurrentMerchantId()));
					}
					if (null != vo.getCurrentMerchantStaffId()) {
						builder.header(BcbConstant.HEADER_CURRENT_MERCHANT_STAFF_ID, String.valueOf(vo.getCurrentMerchantStaffId()));
					}

					// 向后面微服务转发时，删除token头，以避免token泄露
					builder.headers(x -> {
						x.remove(AUTHORIZE_TOKEN);
					});

		            ServerWebExchange mutableExchange = exchange.mutate().request(builder.build()).build();
					return chain.filter(mutableExchange);
				}
			} else {
				log.warn("Invalid token.");
				return unauthorizedResponse(exchange, ResultVO.error(UsrAuthCheckResultEnum.USR_ERROR_TOKEN_INVALID_FORMAT.getCode(),
						                                             UsrAuthCheckResultEnum.USR_ERROR_TOKEN_INVALID_FORMAT.getMsg()));
			}

        } catch (Exception e) {
			log.error("Gateway exception:{}.", e.toString());
			return unauthorizedResponse(exchange, ResultVO.error(UsrAuthCheckResultEnum.USR_ERROR_AUTH_OTHER.getCode(),
					                                             UsrAuthCheckResultEnum.USR_ERROR_AUTH_OTHER.getMsg()));
        }
    }

    /**
     * @description: 请求地址是否为需要忽略认证鉴权的地址
     * @param servletPath: 请求地址
     * @return: boolean
     */
	private boolean isIgnoreHttpUrls(String servletPath) {
		PathMatcher pathMatcher = new AntPathMatcher();

		boolean rt = false;
		List<String> ignoreHttpUrls = ignoreUrlsProperties.getHttpUrls();
		for (String ihu : ignoreHttpUrls) {
			if (pathMatcher.match(ihu, servletPath)) {
				rt = true;
				break;
			}
		}
		return rt;
	}

    /**
     * @description: 认证鉴权失败时，返回json数据
     * @param exchange: 请求消息包
     * @param vo: 待返回的ResultVO数据
     * @return: reactor.core.publisher.Mono<java.lang.Void>
     */
    private Mono<Void> unauthorizedResponse(ServerWebExchange exchange, ResultVO vo){
        ServerHttpResponse originalResponse = exchange.getResponse();
        originalResponse.setStatusCode(HttpStatus.OK);
        originalResponse.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        byte[] response = null;
        try{
            response = JSON.toJSONString(vo).getBytes(UTF8);
        }catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
        DataBuffer buffer = originalResponse.bufferFactory().wrap(response);
        return originalResponse.writeWith(Flux.just(buffer));
    }
    
    @Override
    public int getOrder() {
		return -100;
    }

}